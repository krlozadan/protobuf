import json
from protobuf import addressbook_pb2

data = {
    'id': 123,
    'name': 'Carlos Cortes',
    'email': 'krlozadan@gmail.com',
    'phones': [
        {
            'number': '555-555-5555',
            'type': 0
        },
        {
            'number': '777-777-7777',
            'type': 1
        }
    ]
}

def save_protobuf_data():
    address_book = addressbook_pb2.AddressBook()

    # Creates the person and adds it to the address book
    person = address_book.people.add();
    person.id = data['id']
    person.name = data['name']
    person.email = data['email']

    # Adds phone numbers to the person
    for phone_entry in data['phones']:
        phone = person.phones.add();
        phone.number = phone_entry['number']
        phone.type = phone_entry['type']
    with open('data.bin', 'wb') as opened_file:
        opened_file.write(address_book.SerializeToString());

def save_json_data():
    with open('data.json', 'w') as opened_file:
        opened_file.write(json.dumps(data));

def read_data():
    with open('data.json', 'r') as json_file:
        j = json_file.read()
        print("JSON file is " + str(len(j)) + " bytes")
        print(j)
    with open('data.bin', 'rb') as pb_file:
        address_book = addressbook_pb2.AddressBook()
        address_book.ParseFromString(pb_file.read())
        print("Protobuf file is " + str(address_book.ByteSize()) + " bytes")
        print(str(address_book))

save_protobuf_data()
save_json_data()
read_data()

