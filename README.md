# Protobuf Test

To run the code:

1. Make sure you have Python installed
2. inside the root folder run the following to create a [virtual environment](https://docs.python.org/3/library/venv.html)
```powershell
python -m venv ./.venv
```
3. Activate the virtual environment by running:
```powershell
./.venv/Scripts/activate
```
4. Install the project dependencies by running:
```powershell
pip install -r ./requirements.txt
```
5. Run the project:
```powershell
python protobuf.py
```

To deactivate the virtual environment run:
```powershell
deactivate
```
